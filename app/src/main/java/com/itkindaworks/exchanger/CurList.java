package com.itkindaworks.exchanger;

import java.util.ArrayList;

/**
 * Created by remember me on 22.06.2017.
 */

public class CurList {

    private static ArrayList<Currency> mCurrencies;

    CurList(){
        mCurrencies = new ArrayList<>();
    }

    public static ArrayList<Currency> getCurrencies() {
        return mCurrencies;
    }

    public void setCurrencies(Currency currency) {
        mCurrencies.add(currency);
    }
}
