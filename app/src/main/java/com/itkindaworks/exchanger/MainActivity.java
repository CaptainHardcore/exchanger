package com.itkindaworks.exchanger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    private AsyncLoader mAsyncLoader = new AsyncLoader();
    private XmlPullParser xmlPullParser = null;
    private Currency currency;
    private TextView mTextView;
    private int counter = 0;
   public static CurList mCurList;
     private Button mButton;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mTextView = (TextView) findViewById(R.id.text);
        mCurList = new CurList();
        mButton = (Button)findViewById(R.id.button);
        mButton.setText(R.string.ready);
        mButton.setClickable(false);




      }



        XmlPullParser prepareXpp() throws XmlPullParserException, ExecutionException, InterruptedException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
                try {
                    xpp.setInput(new StringReader(
                            mAsyncLoader.get()));
                } catch (XmlPullParserException | InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                Log.i("TAG", "extrcting");

        return xpp;
    }

    public void onClicked(View view){
        Intent intent = new Intent(MainActivity.this, ResultActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mAsyncLoader.execute();
        XmlPullParser xmlPullParser = null;
        try {
            xmlPullParser = prepareXpp();
        } catch (XmlPullParserException | ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
        Log.i("TAG", "RAPSING");


        try {
            assert xmlPullParser != null;
            while (xmlPullParser.getEventType() != XmlPullParser.END_DOCUMENT) {

                Log.i("TAG", "NOT END");

                switch (xmlPullParser.getEventType()) {
                    case XmlPullParser.START_TAG:
                        if (xmlPullParser.getName().equals("Valute")) {
                            currency = new Currency();
                            currency.setNumber(counter);
                            Log.i("TAG", "CREATING NEVV OBJ");

                        }
                        if (xmlPullParser.getName().equals("CharCode")) {
                            currency.setCharcode(xmlPullParser.nextText());


                        }

                        if (xmlPullParser.getName().equals("Nominal")){
                            currency.setNominal(Integer.parseInt(xmlPullParser.nextText()));
                            Log.i("TAG", String.valueOf(currency.getNominal()));
                        }
                        if (xmlPullParser.getName().equals("Name")){
                            currency.setName(xmlPullParser.nextText());
                            Log.i("TAG", currency.getName());
                        }
                        if (xmlPullParser.getName().equals("Value")){


                            currency.setValue(Double.parseDouble(xmlPullParser.nextText().replace(",",".")));
                            Log.i("TAG", String.valueOf(currency.getValue()));
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (xmlPullParser.getName().equals("Valute")){
                            mCurList.setCurrencies(currency);
                            currency = null;
                            counter++;

                            Log.i("TAG", "VALUTE OFF");
                        }


                }
                xmlPullParser.next();



            }
            mButton.setClickable(true);
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }

    }
}
