package com.itkindaworks.exchanger;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by remember me on 22.06.2017.
 */

public class ResultView  extends AppCompatActivity{
    private int from;
    private int to;
    private double amount;
    private double fromToRubles;
    private double roublesTo;

    private TextView mTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_view);

        Intent intent = getIntent();
        from = Integer.parseInt(intent.getStringExtra("from"));
        to = Integer.parseInt(intent.getStringExtra("to"));
        amount = Double.parseDouble(intent.getStringExtra("amount"));

        fromToRubles = CurList.getCurrencies().get(from).getValue()/CurList.getCurrencies().get(from).getNominal()*amount;
        roublesTo = fromToRubles/CurList.getCurrencies().get(to).getValue()/CurList.getCurrencies().get(to).getNominal();

        mTextView = (TextView)findViewById(R.id.result);
        mTextView.setText(String.valueOf(roublesTo));


    }
}
