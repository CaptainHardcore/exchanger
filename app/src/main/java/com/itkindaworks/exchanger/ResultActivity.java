package com.itkindaworks.exchanger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    private TextView mTextView;
    private EditText mEditText;

    private int from;
    private int to;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        final Spinner spinner =(Spinner)findViewById(R.id.spinner);
        final Spinner spinner2 =(Spinner)findViewById(R.id.spinner2);
        mEditText = (EditText)findViewById(R.id.amount);
        HardcodeData data = new HardcodeData();

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data.getData());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
        spinner2.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

               from = position;
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                to = position;
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });









    }
    public void onResult(View view){
        Intent intent = new Intent(ResultActivity.this, ResultView.class);
        intent.putExtra("from", String.valueOf(from));
        intent.putExtra("to", String.valueOf(to));
        intent.putExtra("amount", mEditText.getText().toString() );
        startActivity(intent);

    }

}
