package com.itkindaworks.exchanger;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by remember me on 21.06.2017.
 */

class AsyncLoader extends AsyncTask<Void, Void, String> {

    StringBuilder builder;


    @Override
    protected String doInBackground(Void... voids) {

        try {
            URL url = new URL("http://www.cbr.ru/scripts/XML_daily.asp");
            URLConnection connection = url.openConnection();
            Log.i("TAG", "open connection");
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()) );
            builder = new StringBuilder();
            String line;

            while (!(line = reader.readLine()).equals("</ValCurs>")) {
                builder.append(line);
                Log.i("TAG", line);}}

        catch (IOException e) {
            e.printStackTrace();}

        return builder.toString();
    }

   
}
